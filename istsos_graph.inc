<?php

// Callback functionalities to render graph of selected procedures

//
// Get procedure details to fill the multiselect jquery ui widget
//

function get_procedure_details(){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	// $offe_name = $_POST['offename'];
	$prc_name = $_POST['procname'];

	//
	// Call to proceduredetails to create a string of observerved properties
	//
	$procedure = istsos_REST_proceduredetails ($url, $service, $prc_name);

	if ($procedure) {
			// $outputscount = count($procedure['data']['outputs']) - 1;
			foreach ( $procedure['data']['outputs'] as $key => $value ){
				if ($key == 0) { // time with constraint
					$eventtime['start'] = $value['constraint']['interval'][0];
					$eventtime['end'] = $value['constraint']['interval'][1];
				} else {
					// $ob_prop_name[] = $value['name'];
					// $ob_prop_def[]  = $value['definition'];
					$ob_prop[$value['name']] = $value['definition'];
				}
			}
			$rv["ob_prop"] = $ob_prop;
			$rv["eventtime"] = $eventtime;
			drupal_json_output($rv);
	} else {
		echo "Procedure error";
	}
}

/**/
/* Parameters
/* $url			   - global
/* $service    - global
/* $offe_name  - name of the offering
/* $prc_name   - name of the procedure
/* $ob_prop    - 'single' observed property uri
/* $start      - start date
/* $end        - end date
/**/

function get_observation_data() {

	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	$offe_name = $_POST['offename'];
	$prc_name = $_POST['procname'];
	$ob_prop = $_POST['opdef'];
	$start = $_POST['start'];
	$end = $_POST['end'];

	$eventtime = array($start, $end);
	// drupal_set_message("istsos_graph.inc ofeering name <pre>".print_r($offe_name,TRUE)."</pre>");
	//
	// Call to getobservation
	//
	$observation = istsos_REST_getobservation ($url, $service, $offe_name, $prc_name, $ob_prop, $eventtime);
	if ($observation) {
			// drupal_set_message($observation['message']);
			drupal_json_output($observation['data'][0]);
	} else {
		drupal_set_message('istsos_REST_getobservation FAILED','error');
	}
}
