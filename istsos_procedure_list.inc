<?php

function istsos_tab($node, $arg1, $arg2){
  plotly_js_load();
  drupal_add_library('system', 'ui.dialog');
  drupal_add_js(drupal_get_path('module', 'istsos') . '/js/istsosutils.js');
  drupal_add_js(drupal_get_path('module', 'istsos') . '/js/istsosplotly.js');
  return drupal_get_form("istsos_list_procedures_form", $node, $arg1, $arg2);
}

function istsos_list_procedures_form($form,&$form_state, $node, $arg1, $arg2){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

  if (!isset($form_state['stage'])) $form_state['stage'] = 'list_procedures';
	switch ($form_state['stage']) {

    case 'list_procedures':
      return istsos_list_procedures_form_prim($form, $form_state, $node, $arg1, $arg2);
     break;

    case 'add_new_procedure':
      return istsos_add_new_procedure_form($form, $form_state, $node, NULL);
     break;

    default:
      return istsos_list_procedures_form_prim($form, $form_state, $node, $arg1, $arg2);
     break;
  }
  return $form;
}

function istsos_list_procedures_form_prim($form,&$form_state, $node, $arg1, $arg2){

	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
  $field   = variable_get('istsos_pg_field');
  // TODO to be removed
  $enodet  = rawurlencode($node->title);  // encoded node title with spaces replaced with %20 for drupal_http_request calls
  // get the srid of the configured postgis field
  $field_info = field_info_field($field);
  $srid = $field_info['settings']['srid'];

  if (isset($service)) {
    // use the Enhanced functionality GetFeatureOfInterest to get a list of procedures attached to the FOI
    $procedures_list = istsos_GetFeatureOfInterest($url, $service, $node->title, $srid);
    // drupal_set_message("prc <pre>".print_r($procedures_list,TRUE)."</pre>");

      $form['existing_procedure'] = array(
        '#markup' => '<h1>istSOS Service : '.$service.' </h1> <a href=?q=admin/config/istsos/services/select>Select another service</a> <h4>Existing procedures for the '.$node->title.' node .</h4>',
      );

      $header = array(
        'proc_name'  => t('Procedure Name'),
        'proc_period'  => t('Observation interval'),
        'proc_op'  => t('Observed properties'),
        //'proc_file' => t('Files'),
        'proc_graph' => t('Graph'),
        'proc_edit'  => t('Edit procedure'),
      );

      $options = array();

      if (isset($procedures_list[$service])){
        foreach ($procedures_list[$service] as $value){
          // Get details about each procedure to fill the table
          $procedure = istsos_REST_proceduredetails($url, $service, $value);
          if ($procedure) {
            // drupal_set_message("prc <pre>".print_r($procedure,TRUE)."</pre>");
            // drupal_set_message($procedure['message']);
            // Set observed properties period
            if(isset($procedure['data']['outputs'][0]['constraint'])){
              $begin = strtotime($procedure['data']['outputs'][0]['constraint']['interval'][0]);
              $end   = strtotime($procedure['data']['outputs'][0]['constraint']['interval'][1]);
              $period = 'Observation from '.format_date($begin, $type = 'medium', $format = '', $timezone = NULL, $langcode = NULL).' to '.format_date($end, $type = 'medium', $format = '', $timezone = NULL, $langcode = NULL);
            } else {
              $begin = 0;
              $end = 0;
              $period = 'No observation inside this procedure.';
            }

            // Set observed properties name list
            $op = '';
            foreach($procedure['data']['outputs'] as $key => $val){
              if ($key != 0){
                $op .= $procedure['data']['outputs'][$key]['name'].'<br />';
              }
            }
          }

          // offename attribute of the anchor - settled to temporary
          $eoffe_name = rawurlencode($node->title);
          $graph_link = ($begin == 0) ? '' : '<a class="graph" offename="temporary" prcname='.$value.' href="#">view</a>';

          $options[$value] =array(
            'proc_name' => array(
              'data' => array(
                '#markup'	=> '<a class="procedure" href="?q=node/'.$node->nid.'/istsos/'.$value.'/view">'.$value.'</a>',
              ),
            ),
            'proc_period'  => array(
              'data' => array(
                '#markup'	=> $period,
              ),
            ),
            'proc_op'  => array(
              'data' => array(
                '#markup'	=> $op,
              ),
            ),
            'proc_graph'  => array(
              'data' => array(
                '#markup'	=> $graph_link,
              ),
            ),
            'proc_edit'  => array(
              'data' => array(
                '#markup'	=> '<a class="procedure" href="?q=node/'.$node->nid.'/istsos/'.$value.'/edit">edit - upload observation</a>',
              ),
            ),
          );
        }
      }

  		$form['procedure_table'] = array(
  	    '#type' => 'tableselect',
  			'#header' => $header,
  	    '#options' => $options,
  			'#empty' => t('No procedures found'),
  		);

  		$form['submit'] = array('#type' => 'submit', '#weight' => 19, '#value' => t('Add New Procedure'), '#submit' => array('istsos_set_stage'));
  		$form['delete'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Delete Selected Procedures'), '#submit' => array('istsos_delete_procedures'));
  		$form['back']   = array  ('#markup'	=> '<a href="?q=node/'.$node->nid.'/istsos/0/0">Back</a>', '#weight' => 21);

  		if ( ($arg2=='view') ){
        $procedure = istsos_REST_proceduredetails ($url, $service, $arg1);
  			if ($procedure) {
          drupal_set_message($procedure['message']);
  				// drupal_add_css(drupal_get_path('module', 'istsos') . '/css/procedure.css');

  	 			/* add form elements with informations about the selected procedure */
  				$form["wrappall"] = array(
  					'#type' => 'fieldset',
  					'#title' => 'Procedure '.$arg1,
  					'#attributes' =>	array('class' => array('wrappall')),
  				);
  	 			$form["wrappall"]["giwrapper"] = array(
  					'#type' => 'fieldset',
  					'#title' => t('General Info'),
  					'#attributes' =>	array('class' => array('division')),
  				);
  				$form["wrappall"]["giwrapper"]['title'] = array('#prefix' => '<div>', '#suffix' => '</div>',	'#markup' => '<b>Name : </b>'.$arg1);
  				$form["wrappall"]["giwrapper"]['pid'] = array('#prefix' => '<div>', '#suffix' => '</div>',	'#markup' => '<b>Sensor ID : </b>'.$procedure['data']['assignedSensorId']);
  			 	$form["wrappall"]["giwrapper"]['description'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>Description : </b>'.$procedure['data']['description']);
  			 	$form["wrappall"]["giwrapper"]['keywords'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>Keywords </b>: '.$procedure['data']['keywords']);

  	 			$form["wrappall"]["clawrapper"] = array(
  					'#type' => 'fieldset',
  					'#title' => t('Classification'),
  					'#attributes' =>	array('class' => array('division')),
  				);
  			 	$form["wrappall"]["clawrapper"]['systype'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>System Type : </b>'.$procedure['data']['classification'][0]['value']);
  			 	$form["wrappall"]["clawrapper"]['senstype'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>Sensor Type : </b>'.$procedure['data']['classification'][1]['value']);

  			 	$form["wrappall"]['location'] = array(
  					'#type' => 'fieldset',
  					'#title' => t('Location'),
  					'#attributes' =>	array('class' => array('division')),
  				);
  			 	$form["wrappall"]['location']['foiname'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>FOI name : </b>'.$procedure['data']['location']['properties']['name']);
  				$form["wrappall"]['location']['srid'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>EPSG : </b>'.$procedure['data']['location']['crs']['properties']['name']);
  				$form["wrappall"]['location']['st_x'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>X : </b>'.$procedure['data']['location']['geometry']['coordinates'][0]);
  				$form["wrappall"]['location']['st_y'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>Y : </b>'.$procedure['data']['location']['geometry']['coordinates'][1]);
  				$form["wrappall"]['location']['st_z'] = array('#prefix' => '<div>', '#suffix' => '</div>', '#markup' => '<b>Z : </b>'.$procedure['data']['location']['geometry']['coordinates'][2]);

  				$form["wrappall"]['outputs'] = array(
  					'#type' => 'fieldset',
  					'#title' => t('Outputs'),
  					'#attributes' =>	array('class' => array('newline')),
  				);

  				$header = array(
  					'out_name'  => t('Name'),
  					'out_descr' => t('Description'),
  					'out_def'   => t('Definition'),
  					'out_uom'   => t('Unit of Measure'),
  				);

  				$options = array();
  				foreach($procedure['data']['outputs'] as $key => $value) {
  					if ($key != 0) {
  						$options[$key] =array(
  							'out_name'  => $value['name'],
  							'out_descr' => $value['description'],
  							'out_def'   => $value['definition'],
  							'out_uom'   => $value['uom'],
  						);
  					}
  				}

  				$form["wrappall"]['outputs']['out_table'] = array(
  					'#theme' => 'table',
  					'#header' => $header,
  					'#rows' => $options,
  					'#empty' => t('No observed propeties found'),
  				);
  			} else {
  				drupal_set_message("Can't get information about istSOS server", 'error');
  			}
  			$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Add New Procedure'), '#submit' => array('istsos_set_stage'));
  		}
  		/* end VIEW PROCEDURE */

      // $form['test_multiselect'] = array(
      //   '#markup' => '<select multiple=multiple></select>',
      //   // '#markup' => '<select class="onlydrupal" multiple=multiple> <option value="1">Option 1</option><option value="2">Option 2</option><option value="3">Option 3</option><option value="4">Option 4</option><option value="5">Option 5</option></select>',
      // );

      /* DIALOG WINDOW FOR GRAPH */
      $form["wrapgraph"] = array(
        '#type' => 'fieldset',
        // '#title' => 'Procedure ',
        '#attributes' =>	array('id' => array('graphDW'), 'class' => array('wrapgraph'), 'hidden' => array('true')),
      );
      $form["wrapgraph"]['test_checkbox'] = array(
        '#markup' => '<div id="check" style="display:inline; width:20%; float:left; overflow-y: scroll;"></div>',
      );
      $form["wrapgraph"]['test_plotly'] = array(
        '#markup' => '<div id="plotly" style="display:inline; widht:80%; float:right;"></div>',
      );
      /* END DIALOG */

  		/* EDIT PROCEDURE */
  		if ( ($arg2 == 'edit') ){
  			unset($form['procedure_table']);
  			unset($form['existing_procedure']);
  			unset($form['delete']);
  			$form = istsos_add_new_procedure_form($form, $form_state, $node, $arg1);
  		}
		/* end EDIT PROCEDURE */
  } else {
    drupal_set_message('No Service selected', 'error');
  }
	return $form;
}

/**/
/* Define the form for insert or update a procedure */
/**/
function istsos_add_new_procedure_form($form,&$form_state, $node, $arg1){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

	$enc_proc_name = rawurlencode($arg1);

  $form['#prefix'] = '<div id="wizard-form-wrapper">';
  $form['#suffix'] = '</div>';
  $form['#tree'] = TRUE; // We want to deal with hierarchical form values.

	$step = empty($form_state['storage']['step']) ? 1 : $form_state['storage']['step'];
  $form_state['storage']['step'] = $step;

	if (isset($arg1)){
		$procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures/'.$enc_proc_name);
					if (in_array( $procedure->code, array(200, 304))) {
						 $t = drupal_json_decode($procedure->data);
						 if ($t['success']){
#						 	drupal_set_message("Update <pre>".print_r($procedure,TRUE)."</pre>");
						 }
					}
		$form['update'] = array('#type' => 'hidden', '#value' => true);
	}

	$form['node_title'] = array('#type' => 'hidden', '#value' => $node->title);
	$form['node_nid'] = array('#type' => 'hidden', '#value' => $node->nid);

	(empty ($arg1)) ? $form['form_title'] = array('#markup' => '<h1>Please fill the form in order to create a new procedure</h1>') : $form['form_title'] = array('#markup' => '<h1>Update procedure '.$arg1.' form</h1>')	;

		$form['gi'] = array(
			'#type' => 'fieldset',
			'#title' => t('General Info'),
			'#attributes' => array('class' => array('container-inline')),
		);
		if ( ! empty($t['data']['assignedSensorId']) ) {
			$form['gi']['SensorId'] = array(
				'#markup' => 'Sensor ID: '.$t['data']['assignedSensorId'],
			);
			$form['assignedSensorId'] = array('#type' => 'hidden', '#value' => $t['data']['assignedSensorId']);
		}


	 	$form['gi']['title'] = array(
			'#type' => 'textfield',
			'#title' => t('Name'),
			'#default_value' => (empty ($arg1)) ? $node->title : $arg1,
			'#size' => 60,
			'#disabled' => (empty ($arg1)) ? FALSE : TRUE, // If we have to update the procedure modify the procedure name give some errors from istSOS server
			'#required' => TRUE,
		);
	 	$form['gi']['description'] = array(
			'#type' => 'textfield',
			'#title' => t('Description'),
			'#default_value' => (empty ($t['data']['description'])) ? '' : $t['data']['description'],
			'#size' => 60,
		);
	 	$form['gi']['keywords'] = array(
			'#type' => 'textfield',
			'#title' => t('Keywords'),
			'#default_value' => (empty ($t['data']['keywords'])) ? '' : $t['data']['keywords'],
			'#size' => 60,
		);

		$form['cla'] = array(
			'#type' => 'fieldset',
			'#title' => t('Classification'),
			'#attributes' => array('class' => array('container-inline')),
		);
	 	$form['cla']['systype'] = array(
			'#type' => 'textfield',
			'#title' => t('System Type'),
			'#default_value' => (empty ($t['data']['classification'][0]['value'])) ? 'insitu-fixed-point' : $t['data']['classification'][0]['value'],
			'#size' => 60,
			'#disabled' => TRUE,
		);
	 	$form['cla']['senstype'] = array(
			'#type' => 'textfield',
			'#title' => t('Sensor Type'),
			'#default_value' => (empty ($t['data']['classification'][1]['value'])) ? '' : $t['data']['classification'][1]['value'],
			'#size' => 60,
			'#required' => TRUE,
		);

		$form['location'] = array(
			'#type' => 'fieldset',
			'#title' => t('Location'),
		);
	 	$form['location']['foiname'] = array(
			'#type' => 'textfield',
			'#title' => 'FOI name',
			'#default_value' => (empty ($t['data']['location']['properties']['name'])) ? $node->title : $t['data']['location']['properties']['name'],
			'#size' => 60,
			'#disabled' => TRUE,
		);

		/* Calculate the coordinates from the postgis field in the node */
		$n_type = $node->type;
		// Get the postgis field name given the node type
		$field_info_map = field_info_field_map();
		foreach ($field_info_map as $key => $value){
			if ( (isset ($value['bundles']['node'])) && ($value['bundles']['node'][0] == $n_type) && ($value['type'] == 'postgis') ){
				$field_name = $key;
			}
		}
		$field_info = field_info_field($field_name);

		$srid = $field_info['settings']['srid'];

		$form['location']['srid'] = array(
			'#type' => 'textfield',
			'#title' => 'EPSG',
			'#description' => 'The EPSG value is extracted from the field settings.',
			'#default_value' => (empty ($t['data']['location']['crs']['properties']['name'])) ? $srid : $t['data']['location']['crs']['properties']['name'],
			'#size' => 60,
			'#disabled' => TRUE,
		);

		$result = db_query("select ST_X(".$field_name."_geometry), ST_Y(".$field_name."_geometry), ST_AsText(".$field_name."_geometry) from field_data_".$field_name." where entity_id=".$node->nid.";");
		$db_entry = $result->fetchAssoc();
#		drupal_set_message("db results <pre>".print_r($db_entry,TRUE)."</pre>");

		$form['location']['st_x'] = array('#type' => 'textfield', '#title' => 'X', '#default_value' => $db_entry['st_x'], '#disabled' => TRUE);
		$form['location']['st_y'] = array('#type' => 'textfield', '#title' => 'Y', '#default_value' => $db_entry['st_y'], '#disabled' => TRUE);
		$form['location']['st_z'] = array('#type' => 'textfield', '#title' => 'Z', '#default_value' => (empty ($t['data']['location']['geometry']['coordinates'][2])) ? '' : $t['data']['location']['geometry']['coordinates'][2], '#required' => TRUE);

		/******************************************/
		/*             OUTPUTS                    */
		/******************************************/
		$form['outputs'] = array(
			'#type' => 'fieldset',
			'#title' => t('Outputs'),
			'#attributes' => array('class' => array('container-inline')),

		);

		/* List of observerved properties */
		/* Presents procedures outputs */

				$output_header = array(
					'output_name'  => t('Name'),
					'output_def'  => t('Definition'),
					'output_uom'  => t('Unit of Measure'),
				);


				if(isset($form['update']) && ($form['update']["#value"])){

				$view_procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures/'.$enc_proc_name);

					if (in_array( $view_procedure->code, array(200, 304))) {
						 $t = drupal_json_decode($view_procedure->data);
						 if ($t['success']){
						 $output_options = array();

									foreach($t['data']['outputs'] as $key => $value) {
										if ($key != 0) {
											$output_options[$key] =array(
												'output_name'  => $value['name'],
												'output_def'   => $value['definition'],
												'output_uom'   => $value['uom'],
											);
											$form['old_outputs'][$key] = array('#type' => 'hidden', '#value' => serialize($output_options[$key]));
										}
									}
							} else {
								drupal_set_message("Something wrong getting the procedure data.", 'error');
							}
					} else {
						drupal_set_message("Can't get information about istSOS server", 'error');
					}



				} else {
					$output_options = array();
					if (!empty ($form_state['storage']['values'])){
						foreach ($form_state['storage']['values'] as $step => $value){
							$output_options[$step] =array(
								'output_name' => $form_state['storage']['values'][$step]['new_ob_property'],
								'output_def'  => $form_state['storage']['values'][$step]['new_ob_property'],
								'output_uom'  => $form_state['storage']['values'][$step]['new_uom']

							);
						}
					}
				}


				$form['outputs']['output_table'] = array(
			    '#theme' => 'table',
					'#header' => $output_header,
			    '#rows' => $output_options,
					'#empty' => t('No observed properties found'),
					'#attributes' => array('id' => 'istsos_outputs_table'),
				);

		/* Get a list of observed properties */
		$obprop = drupal_http_request($url.'/wa/istsos/services/'.$service.'/observedproperties');
		if (in_array( $obprop->code, array(200, 304))) {
			$t = drupal_json_decode($obprop->data);
			foreach($t['data'] as $key => $value) {
					$op_options[$value['definition']] =  $value['name']; // array [urn observed property] = name
					$form['outputs']['op_def'][$value['definition']] = array('#type' => 'hidden', '#value' => $value['name']);
			}
		} else {
			drupal_set_message('Observed Properties istSOS service not reachable', 'error');
		}

		/* Get a list of unit of measure */
		$uom = drupal_http_request($url.'/wa/istsos/services/'.$service.'/uoms');
		if (in_array( $uom->code, array(200, 304))) {
			$t = drupal_json_decode($uom->data);
			foreach($t['data'] as $key => $value) {

					$uom_options[$value['name']] =  $value['name']; // array [urn observed property] = name
					$form['outputs']['op_uom'][$value['name']]= array('#type' => 'hidden', '#value' => $value['name']);
			}
		} else {
			drupal_set_message('Unit of Measures istSOS service not reachable', 'error');
		}

		if ( empty($arg1) ) {

				$form['outputs']['new_ob_property'] = array(
					'#type' => 'select',
					'#title' => t('Observed property'),
					'#options' => $op_options,

				);
				$form['outputs']['new_uom'] = array(
					'#type' => 'select',
					'#title' => t('Unit of measure'),
					'#options' => $uom_options,
				);

				$form['outputs']['add_op'] = array(
					'#type' => 'submit',
					'#value' => t('Add this Output'),
					'#submit' => array('istsos_add_op_submit'),
					'#ajax' => array(
		  		  'callback' => 'ajax_form_multistep_form_ajax_callback',
				    'wrapper' => 'wizard-form-wrapper',
				  ),
				);

		} else if ( empty($op_options) || empty($uom_options) ){
			drupal_set_message("Observed properties or Unit of measures are not present.<br />Please insert an Observerd Property and a Unit of Measure at least using the istSOS configuration panel.", 'error');
		}

		if (isset($arg1)){
	 		$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Update procedure'), '#submit' => array('istsos_update_procedure'));
      $form['csvupload'] = array(
        '#title' => t('Upload observations file for this procedure'),
        '#type' => 'managed_file',
        // '#upload_validators' => array('file_validate_extensions' => array('csv'), 'sistems_validate_file_content' => array('void'), ),
        '#upload_validators' => array('file_validate_extensions' => array('csv'), ),
        '#upload_location' => 'public://'.$node->nid,
      );
	 		$form['back']   = array  ('#markup'	=> '<a href="?q=node/'.$node->nid.'/istsos/0/0">Back</a>', '#weight' => 30);
		} else {
	 		$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Add procedure'), '#submit' => array('istsos_add_procedure'));
	 		$form['#validate'][] = 'istsos_add_procedure_validate';
      $form['back']   = array  ('#markup'	=> '<a href="?q=node/'.$node->nid.'/istsos/0/0">Back</a>', '#weight' => 30);
	 	}

  return $form;
}


function istsos_add_op_submit($form, &$form_state) {
	$current_step = 'step' . $form_state['storage']['step'];
	if (!empty($form_state['values']['outputs'])) {
    $form_state['storage']['values'][$current_step] = $form_state['values']['outputs'];
  }
  if ($form_state['triggering_element']['#value'] == t('Add this Output')) {
    $form_state['storage']['step']++;
    $step_name = 'step' . $form_state['storage']['step']; // eg. 1
    // If values have already been entered for this step, recover them from
      $urn = $form_state['values']['outputs']['new_ob_property'];
      $uom = $form_state['values']['outputs']['new_uom'];
 		  $form_state['values'][$step_name] = array($urn,$uom);
  }
	$form_state['rebuild'] = TRUE;
}


function ajax_form_multistep_form_ajax_callback($form, $form_state){
	 return $form;
}

/***********************************************************************************************/
	/*  Uodate a procedure, we need the following parameters */
	/* TODO  Add all the other configuration parameters */
	/* - Name - Procedure name - Actually we use the node name */
	/* - System type - Actually just insitu-fixed-point */
	/* - Sensor type - Description of the sensor*/
	/* - FOI name - Actually we use the node name */
	/* - EPSG - just the default 4326 */
	/* - Coordinates - The ones from the postgis field in the node */

	/* The output fields */
	/* Time is mandatory */
/***********************************************************************************************/


function istsos_update_procedure ($form,&$form_state){
  $url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
  // drupal_set_message("<pre>".print_r($form_state['values'],TRUE)."</pre>");

	//extract the numeric value of EPSG
	$appo = explode(':',$form_state['values']['location']['srid']);
	$srid = $appo[1];


	/* Update a present procedure */
		$data = array();
		$data['system_id'] = $form_state['values']['gi']['title']; // Using the procedure name for this parameter as is using the istSOS admin interface.
    $data['system'] = $form_state['values']['gi']['title'];    // Procedure name
    $data['description'] = $form_state['values']['gi']['description'];
    $data['keywords'] = $form_state['values']['gi']['keywords'];
    $data['identification'] = array();
    $data['classification']	= array(
    	array ("name" => "System Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:systemType", "value" => $form_state['values']['cla']['systype']),
    	array ("name" => "Sensor Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:sensorType", "value" => $form_state['values']['cla']['senstype']),
    );
    $data['characteristics'] = "";
    $data['contacts'] = array();
    $data['documentation'] = array();
		$data['location'] = array (
            "type" => "Feature",
            "geometry" => array(
                "type" => "Point",
                "coordinates" => array($form_state['values']['location']['st_y'],$form_state['values']['location']['st_x'],$form_state['values']['location']['st_z']), // TODO Why have to insert the coordinates inverted ???
            ),
            "crs" => array (
                "type" => "name",
								 "properties" => array("name" => $srid),
            ),
            "properties" => array("name" => $form_state['values']['node_title'])
     );
		 $data['interfaces'] = "";
     $data['inputs'] = array();
     $data['history'] =  array();
     $data['capabilities'] =  array();

		 $data['outputs'] = array();
     $time = array(
                "name" => "Time",
								"definition" => "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
                "uom" => "iso8601",
                "description" => "",
     );
		 array_push($data['outputs'], $time);


		 /* Insert old outputs */
     foreach ($form_state['values']['old_outputs'] as $value ){
				$val = unserialize($value);
		   	$output = array(
		      "name" => $val['output_name'],
		      "definition" => $val['output_def'],
		      "uom" => $val['output_uom'],
		      "description" => "", //TODO
		   	);
   			array_push($data['outputs'], $output);
     }

    /* PUT operation to Update the procedure - this modify only the cfg file of the procedure */
		$options = array(
                'method' => 'PUT',
                'data' => drupal_json_encode($data),
                'headers' => array('Content-Type' => 'application/json'),
        );
    $update_procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures/'.$form_state['values']['gi']['title'], $options);

    if (in_array( $update_procedure->code, array(200, 304))) {
				$t = drupal_json_decode($update_procedure->data);
		#		  drupal_set_message("remove procedure <pre>".print_r($t,TRUE)."</pre>");
					drupal_set_message($t['message']);

		/* This step is not implemented using the PUT operation */
		/* we are going to modify the content of the database schema in order to set the new Z value*/
			$result = db_query("SELECT id_foi_fk FROM ".$service.".procedures WHERE assignedid_prc='".$form_state['values']['assignedSensorId']."';");
			$db_entry = $result->fetchAssoc();

			$result = db_query("UPDATE ".$service.".foi SET (geom_foi) = (ST_GeomFromEWKT('SRID=".$srid.";POINT(".$form_state['values']['location']['st_y']." ".$form_state['values']['location']['st_x']." ".$form_state['values']['location']['st_z'].")'))  WHERE id_foi='".$db_entry['id_foi_fk']."';");


		} else {
			drupal_set_message("Can't get information about istSOS server", 'error');
		}

    /**/
    /* Insert observation from CSV */
    /**/
    if ( isset( $form_state['values']['csvupload'] ) ) {
      $config = istsos_REST_configsections($service);
      if ($config !== false){
        $urn_auth = $config['data']['urn']['parameter'];
      }
                                                                // offering name                      // procedure name
      $getobs_res = istsos_REST_getobservation ($url, $service, 'temporary', $form_state['values']['gi']['title'], $urn_auth, 'last');
      if ($getobs_res !== false ){
        #				drupal_set_message("get observation <pre>".print_r($getobs_res,TRUE)."</pre>");
        $skeleton = $getobs_res['data'][0];
        //create fixed context array with ordered elements field from the getobservation request
        foreach ($skeleton['result']['DataArray']['field'] as $index => $dnu) {
          if ($index !== 0) { // jump time element
            if ($index & 1){ // is not quality index
              $order[$dnu['definition']] = $index;
            }
          } else {
            $order['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'] = 0;
          }
        }
      } else {
        drupal_set_message('ERROR getting observation - see log for details', 'error');
      }

      // Load the file
	    $file = file_load($form_state['values']['csvupload']);
      if ( $handle = fopen(drupal_realpath($file->uri), 'r') ) {
        $row = 0;
        while ( $line = fgetcsv($handle, 4096, ',') ) {
          // mantain the observed properties names divided from observations values
          if ($row == 0){
            $ob_prop = $line;
            $row = 1;
          } else {
            $observations[] = $line;
          }
        }
        fclose($handle);
        // insert observations must respect the order of fields of a get observation request
        $ins_obs_resp = istsos_REST_insertobservation($url, $service, $form_state['values']['gi']['title'], $form_state['values']['assignedSensorId'], $observations, $skeleton, $order, $ob_prop);
      }
    }
			unset($_GET['destination']);
			$form_state['redirect'] = 'node/'.$form_state['values']['node_nid'].'/istsos/'.$form_state['values']['gi']['title'].'/view';
}

/* set the stage to present the right form */

function istsos_set_stage ($form,&$form_state){
	$form_state['stage'] = 'add_new_procedure';
	$form_state['rebuild'] = TRUE;
}

function istsos_delete_procedures ($form,&$form_state){
  $url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

  foreach ($form_state['values']['procedure_table'] as $value) {
  	if ($value != '0'){
	 		/* DELETE operation to remove the procedure */

	 		$enc_proc_name = rawurlencode($value);
			$options = array(
		    'method' => 'DELETE',
		    'headers' => array('Content-Type' => 'application/json'),
		  );

		  $remove_procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures/'.$enc_proc_name, $options);
		  $t = drupal_json_decode($remove_procedure->data);
		  drupal_set_message($t['message']);
  	}
  }
}

/***********************************************************************************************/
	/*  Defining a new procedure, we need the following parameters */
	/* TODO  Add all the other configuration parameters */
	/* - Name - Procedure name - Actually we use the node name */
	/* - System type - Actually just insitu-fixed-point */
	/* - Sensor type - Description of the sensor*/
	/* - FOI name - Actually we use the node name */
	/* - EPSG - just the default 4326 */
	/* - Coordinates - The ones from the postgis field in the node */

	/* The output fields */
	/* Time is mandatory */
/***********************************************************************************************/

/* We are going to create a new "procedure" from the node title and its "offering" if it does not exist */

function istsos_add_procedure_validate ($form, &$form_state){
  $url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

	// Grant the unicity of the procedure name
  $procedures = istsos_REST_proceduregetlist($url, $service);
  if (strpos ($procedures['message'], 'success')){
    if ($procedures['data'] != NULL){
      foreach ($procedures['data'] as $value){
          if ($value['name'] == $form_state['values']['gi']['title']){
            form_set_error('gi][title', t('Procedure name already in use. Choose another name for the procedure.'));
          }
      }
    }
  } else {
    form_set_error('form', "Can't get information about the procedure");
  }
  // verify if the service contain the SRID of the configured postgis field
  $srid = trim($form_state['values']['location']['srid']);
  $config = istsos_REST_configsections($service);
  // drupal_set_message("<pre>".print_r($config,TRUE)."</pre>");
  $allowedepsg = str_getcsv($config['data']['geo']['allowedepsg']);
  array_push($allowedepsg, $config['data']['geo']['istsosepsg']);
  if (! in_array($srid, $allowedepsg)){
    // drupal_set_message("not in array");
    form_set_error('form', t('The srid of the feature of interest is not allowed inside this service'));
  }
}

function istsos_add_procedure ($form, &$form_state){
  $url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

	/* Add a new procedure */
	$data['system_id'] = $form_state['values']['gi']['title']; // Using the procedure name for this parameter as is using the istSOS admin interface.
  $data['system'] = $form_state['values']['gi']['title'];    // Procedure name
  $data['description'] = $form_state['values']['gi']['description'];
  $data['keywords'] = $form_state['values']['gi']['keywords'];
  $data['identification'] = array();
  $data['classification']	= array(
  	array ("name" => "System Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:systemType", "value" => $form_state['values']['cla']['systype']),
  	array ("name" => "Sensor Type", "definition" => "urn:ogc:def:classifier:x-istsos:1.0:sensorType", "value" => $form_state['values']['cla']['senstype']),
  );
  $data['characteristics'] = "";
  $data['contacts'] = array();
  $data['documentation'] = array();
	$data['location'] = array (
          "type" => "Feature",
          "geometry" => array(
              "type" => "Point",
              "coordinates" => array($form_state['values']['location']['st_y'],$form_state['values']['location']['st_x'],$form_state['values']['location']['st_z']),
          ),
          "crs" => array (
              "type" => "name",
							 "properties" => array("name" => $form_state['values']['location']['srid']),
          ),
          "properties" => array("name" => $form_state['values']['node_title']) //  insert the node title that is the FOI name
   );
	 $data['interfaces'] = "";
   $data['inputs'] = array();
   $data['history'] =  array();
   $data['capabilities'] =  array();

		 $data['outputs'] = array();
     $time = array(
                "name" => "Time",
								"definition" => "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
                "uom" => "iso8601",
                "description" => "",
     );
		 array_push($data['outputs'], $time);

		 /* Insert new outputs */
     foreach ($form_state['storage']['values'] as $step => $value ){

		   	$output = array(
		      "name" => $form_state['storage']['values'][$step]['new_ob_property'],
		      "definition" => $form_state['storage']['values'][$step]['new_ob_property'],
		      "uom" => $form_state['storage']['values'][$step]['new_uom'],
		      "description" => "", // TODO
		   	);
   			array_push($data['outputs'], $output);

     }

		/* POST operation to ADD the procedure */
		$options = array(
      'method' => 'POST',
      'data' => drupal_json_encode($data),
      'headers' => array('Content-Type' => 'application/json'),
    );

    $add_procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures', $options);
    $t = drupal_json_decode($add_procedure->data);
}
