<?php

function istsos_configuration () {
  return drupal_get_form('istsos_configuration_form');
}

function istsos_configuration_form($form, &$form_state) {
  $form["istsos_url"] = array(
		'#type' => 'textfield',
		'#title' => t('Url to reach istSOS installation'),
		'#description' => 'Full url to reach the istSOS server. (eg http://localhost/istsos)',
		'#default_value' => variable_get('istsos_url',''),
	);

  $form["istsos_path"] = array(
		'#type' => 'textfield',
		'#title' => t('Path to istSOS installation'),
		'#description' => 'Path to the istSOS installation. If you followed the <a href="http://istsos.org/en/latest/doc/installation.html" target="_blank">Installation instructions</a> the path is where you unpacked istSOS (/usr/local/istsos).',
		'#default_value' => variable_get('istsos_path',''),
	);

	// Set an array of node type that contains fields of type postgis
	$field_info = field_info_field_map();
	foreach ($field_info as $key => $value){
		if ($value['type'] == 'postgis'){
			$content_type[$value['bundles']['node'][0]] = $value['bundles']['node'][0]." - ". $key;
      $pg_field[$value['bundles']['node'][0]] = $key;
      // $content_type[$value['bundles']['node'][0]] = array('pgf' => $key);
		}
	}
  $form['pg_field'] = array('#type' => 'hidden', '#value' => $pg_field);

	$form['istsos_select_ct'] = array(
       '#type' => 'select',
       '#title' => t('Select content type to apply the istSOS tab.'),
       '#options' => $content_type,
       '#default_value' =>  variable_get('istsos_select_ct',''),
       '#multiple' => FALSE,
       '#size' => count($content_type),
       '#description' => t('Select content type and field which you want to add procedures.<br />For the selected content types will compare a new tab for set up the procedure.'),
   );

    $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
    return $form;
}

function istsos_configuration_form_submit($form, &$form_state){
  // drupal_set_message("Custom <pre>".print_r($form_state['values'],TRUE)."</pre>");
  variable_set('istsos_url',$form_state['values']['istsos_url']);
  variable_set('istsos_path',$form_state['values']['istsos_path']);
  variable_set('istsos_select_ct',$form_state['values']['istsos_select_ct']);
  variable_set('istsos_pg_field',$form_state['values']['pg_field'][$form_state['values']['istsos_select_ct']]);
}
