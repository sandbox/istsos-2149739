<?php

function istsos_add_service () {
  $form = drupal_get_form('istsos_add_service_form');
  $output = drupal_render($form);
  return $output;
}

function istsos_add_service_form($form, &$form_state) {

	$form = array();

  $form["service"] = array(
		'#type' => 'textfield',
		'#title' => t('Service Name'),
		'#description' => 'Name of the new service you are going to instatiate.',
	);

	$form["epsg"] = array(
		'#type' => 'textfield',
		'#title' => t('EPSG'),
		'#description' => 'The EPSG code for this service.',
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
		'#submit' => array('istsos_add_service_submit'),
		'#validate' => array('istsos_validate_service'),
	);

	return $form;

}


/* The istsos WAlib is going to create a database schema with the service name           */
/* To avoid errors the schema name must not be equal to the drupal default database name */
function istsos_validate_service($form,&$form_state){
#	drupal_set_message("VALIDATING");
	global $databases;
	if ( strcasecmp($form_state['values']['service'], $databases['default']['default']['database']) == 0 ){
		form_set_error('service', 'The service name can\'t be the same of database name.');
	}
  if ( preg_match('/\s/',$form_state['values']['service']) ){
    form_set_error('service', 'The service name can\'t contain spaces.');
  }
}

function istsos_add_service_submit($form,&$form_state){
	global $databases;
#	drupal_set_message("Submit <pre>".print_r($databases,TRUE)."</pre>");
  $data = array();
  $data['service'] = $form_state['values']['service'];
  $data['user'] = $databases['default']['default']['username'];
  $data['password'] = $databases['default']['default']['password'];
  $data['dbname'] = $databases['default']['default']['database'];
  $data['host'] = $databases['default']['default']['host'];
  $data['port'] = "5432";// TODO the port is hard coded -
	$data['epsg'] = $form_state['values']['epsg'];

	/* Create a new service according posted parameters this affect the database correctly but does not affect the service configuration file */
	$options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data),
    'headers' => array('Content-Type' => 'application/json'),
  );
	$url = variable_get('istsos_url');

  $response = istsos_REST_servicescreate($url, $data);
  drupal_set_message($response['message']);

	/* Append to the service configuration file the database connection settings */
	$path = variable_get('istsos_path');
	$service_file = $path.'/services/'.$data['service'].'/'.$data['service'].'.cfg';
	$fh = fopen($service_file, 'a') or die("can't open file");
	$stringData = "\n[connection]\ndbname = ".$data['dbname']."\nhost = ".$data['host']."\nuser = ".$data['user']."\npassword = ".$data['password']."\nport = 5432";
	fwrite($fh, $stringData);
	fclose($fh);
}
