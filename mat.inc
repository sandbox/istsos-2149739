<?php

/* matlab text parser */

class mat {

	private $_fileName;
#	private $_lastTag = '';

	public $name;
	public $author;
	public $created;
	public $id_type;
	public $marker_id;
	public $origin;
	public $scenario;
	public $frequency;
	public $signal;
	public $value;
	

#	public $timesteps;
#	public $values;	
	
#	public $operations = array();
	
	public function __construct($fileName) {
		$this->_fileName = $fileName;
		$this->parse();
	}
	
	public function parse() {
		$tab = $this->prepareFile();
#		echo "debug <br /><pre>";
#		print_r ($tab);
#		echo "</pre>";
		foreach($tab as $key => $line){
		
			switch($key) {
			case 8:
				$this->name = $line;
				break;
			case 12:
				$this->author = $line;
				break;
			case 16:
				$this->created = $line;
				break;				
			case 20:
				$this->id_type = $line;
				break;
			case 24:
				$this->marker_id = $line;
				break;												
			case 39:
				$this->origin = $line;
				break;
			case 43:
				$this->scenario = $line;
				break;
			case 47:
				$this->frequency = $line;
				break;
			case 55:
				$this->signal = $line;
				break;
			case 63:
				$this->value = $line;
				break;				
			default:
				break;
			}
		}
	
#	echo "object <br /><pre>";
#	print_r ($this);
#	echo "</pre>";
	
	
	}

	public function dump() {
		echo '<pre>';
#		var_dump($this);
		print_r($this);
		echo '</pre>';
	}
	

	
	private function prepareFile() {
		$tab = file($this->_fileName);
		$tags = array();
		$tmp = '';
		foreach($tab as $line) {			
#			echo "debug <br /><pre>";
#			print_r ($line);
#			echo "</pre>";
			if ($line{0} == '#' && $tmp != '') {
				$tags[] = $tmp;
				$tmp = '';
			}	
			$tmp .= $line;
		}
		$tags[] = $tmp;
		return $tags;
	}		
}
