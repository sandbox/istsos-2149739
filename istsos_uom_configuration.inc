<?php

function istSOS_uom () {
  $form = drupal_get_form('istSOS_uom_form');
  $output = drupal_render($form);
  return $output;
}

function istSOS_uom_form($form, &$form_state) {
	
	$form = array();

	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	$uom = drupal_http_request($url.'/wa/istsos/services/'.$service.'/uoms');
	if (in_array( $uom->code, array(200, 304))) {
		 $t = drupal_json_decode($uom->data);
#		 drupal_set_message("op <pre>".print_r($t,TRUE)."</pre>");
			if ($t['success']){
				$form['message'] = array(					
					'#markup' => $t['message'],
					'#prefix' => '<div>',
					'#suffix' => '</div>',
				);
				
			/*       LIST OF UNIT OF MEASURE           */
			/*      Use a view with bulk operation     */
				$form['list'] = array('#markup' => "<h2>LIST OF UNIT OF MEASURE</h2>");							
				
				$header = array(
						'uom_name'    => t('Name'),
						'uom_desc'    => t('Description'),
				);
				
				$options = array();
				foreach($t['data'] as $key => $value) {
					$options[$key] = array(
							'uom_name' => $value['name'],
							'uom_desc' => $value['description'],
					);
					
				}
				
				$form['table'] = array(
			  	  '#type' => 'tableselect',
#						'#theme' => 'table',
						'#header' => $header,
			  	  '#options' => $options,
#						'#rows' => $options,
						'#empty' => t('No Unit of Measure found'),
					);
			/*                                         */
			/*        ADD NEW UNIT OF MEASURE 				 */
			/*                                         */
				$form['newuom'] = array(
					'#type' => 'fieldset', 
					'#title' => t('Add Unit of Measure'), 
				);

			 	$form['newuom']['name'] = array(
					'#type' => 'textfield', 
					'#title' => t('Code'),
					'#description' => t('e.g. °C'),
					'#size' => 60, 
				);
			 	$form['newuom']['description'] = array(
					'#type' => 'textfield', 
					'#title' => t('Description'),
					'#description' => t('e.g. Celsius degree'),
					'#size' => 60, 
				);				


			
			}
	}
	else {
		drupal_set_message("Can't get information about istSOS server", 'error');
	}		
		
		$form['newuom']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Add New Unit of Measure'),
			'#submit' => array('istsos_uom_submit'),
		);

		return $form;
		
}

function istsos_uom_submit($form,&$form_state){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	
	$data['name'] = $form_state['values']['name'];
	$data['description'] = $form_state['values']['description'];

	$options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data),
    'headers' => array('Content-Type' => 'application/json'),
  );
	
	$obprop = drupal_http_request($url.'/wa/istsos/services/'.$service.'/uoms', $options);
#	drupal_set_message("INSERT <pre>".print_r($obprop,TRUE)."</pre>");
	if (in_array( $obprop->code, array(200, 304))) {
		 $t = drupal_json_decode($obprop->data);
#		 drupal_set_message("INSERT <pre>".print_r($t,TRUE)."</pre>");
			if ($t['success']){
					drupal_set_message($t['message']);
			}
	}
	else {
		drupal_set_message("Can't get information about istSOS server", 'error');
	}	
}
