<?php

function istsos_observed_properties () {
  return drupal_get_form('istsos_observed_properties_form');
}

function istsos_observed_properties_form($form, &$form_state) {

	$form = array();

	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	$obprop = drupal_http_request($url.'/wa/istsos/services/'.$service.'/observedproperties');
	if (in_array( $obprop->code, array(200, 304))) {
		 $t = drupal_json_decode($obprop->data);

			if ($t['success']){
				$form['message'] = array(
					'#markup' => $t['message'],
					'#prefix' => '<div>',
					'#suffix' => '</div>',
				);
			/*       LIST OF OBSERVED PROPERTIES        */

					$form['list'] = array('#markup' => "<h2>LIST OF OBSERVED PROPERTIES</h2>");

					$header = array(
						'obpr_name'    => t('Name'),
						'obpr_def_urn' => t('Definition URN'),
						'obpr_desc'    => t('Description'),
					);

					$options = array();
					foreach ($t['data'] as $key => $value){
						$options[$key] =array(
			  	    'obpr_name'    => $value['name'],
							'obpr_def_urn' => $value['definition'],
							'obpr_desc'    => $value['description'],
						);
					}

					$form['table'] = array(
			  	  '#type' => 'tableselect',
						'#header' => $header,
			  	  '#options' => $options,
						'#empty' => t('No Observed propeties found'),
					);
			/*                                         */
			/*        ADD NEW OBSERVED PROPERTY				 */
			/*                                         */
				$form['newop'] = array(
					'#type' => 'fieldset',
					'#title' => t('Add Observed Property'),
				);

			 	$form['newop']['name'] = array(
					'#type' => 'textfield',
					'#title' => t('Name'),
					'#description' => t('e.g. air-temperature'),
					'#size' => 60,
				);
			 	$form['newop']['def_urn'] = array(
					'#type' => 'textfield',
					'#title' => t('Definition URN'),
					'#description' => t('Insert only a value like <b>air:temperature</b>.<br />The value inserted here will be prefixed with \'urn:ogc:def:parameter:x-istsos:1.0:\''),
					'#size' => 60,
				);
			 	$form['newop']['description'] = array(
					'#type' => 'textfield',
					'#title' => t('Description'),
					'#description' => t('e.g. air temperature at 2 meters above terrain'),
					'#size' => 60,
				);
				$form['newop']['stat_qi'] = array(
							'#markup' => 'Stat. QI must be implemented',
							'#prefix' => '<div>',
							'#suffix' => '</div>',
						);
				//TODO Stat. QI - constraint must be implemented

			}
	}
	else {
		drupal_set_message("Can't get information about istSOS server", 'error');
	}

		$form['newop']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Add New Observed Property'),
			'#submit' => array('istsos_new_observation'),
		);

		return $form;

}

function istsos_new_observation($form,&$form_state){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');

	$data['name'] = $form_state['values']['name'];
  $data['definition'] = 'urn:ogc:def:parameter:x-istsos:1.0:'.$form_state['values']['def_urn']; // TODO the urn is hard coded -
	$data['description'] = $form_state['values']['description'];
	$data['constraint'] = ''; // TODO manage the constraint

	$options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data),
    'headers' => array('Content-Type' => 'application/json'),
  );

	$obprop = drupal_http_request($url.'/wa/istsos/services/'.$service.'/observedproperties', $options);
#	drupal_set_message("INSERT <pre>".print_r($obprop,TRUE)."</pre>");
	if (in_array( $obprop->code, array(200, 304))) {
		 $t = drupal_json_decode($obprop->data);
#		 drupal_set_message("INSERT <pre>".print_r($t,TRUE)."</pre>");
			if ($t['success']){
					drupal_set_message($t['message']);
			} else {
					drupal_set_message("Somthing wrong inserting the obserbed property.", 'error');
			}
	}
	else {
		drupal_set_message("Can't get information about istSOS server", 'error');
	}
}
