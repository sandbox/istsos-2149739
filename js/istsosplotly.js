(function ($) {

Drupal.behaviors.istsosplotly = {
  attach: function (context, settings) {

    // var windowWidth = ; //retrieve current window width
    // var windowHeight = ; //retrieve current window height
    var dialogWidth = $(window).width()-50;
    var dialogHeight = $(window).height()-200;

    var offeName, procName, opName, start, end;
    var traces = []; // Global container for traces

    $('#plotly').width(dialogWidth-((dialogWidth*25)/100));
    $('#plotly').height(dialogHeight-100);

    $('#graphDW').dialog({
      height: dialogHeight,
      width: dialogWidth,
      modal: true,
      autoOpen: false,
      close: function(event) { // event dialog close
        traces = []; // empty the traces array
        $('#check').empty(); // empty the checkboxes container
        Plotly.purge('plotly'); // purge the graph
      }
    });

    // view link click for given procedure
    $('.graph').click(function(){
      offeName = decodeURIComponent($(this).attr('offename'));
      procName = $(this).attr('prcname');

      $('#graphDW').dialog( "option", "title", procName );
      $('#graphDW').dialog('open');

      $.ajax({
          url: "?q=observedproperties_list_callback",
          type: "POST",
      		// async: false,
          data: {offename: offeName, procname: procName},
          success: obPropListSuccess,
          //error: errorFunction
          // complete: completeFunction,
      });

      function obPropListSuccess (data, textStatus, jqXHR) {
        // console.log(data);
        start = data.eventtime.start;
        end = data.eventtime.end;
        // Fill the options def is the uom urn name is the simlpe value
        $.each(data.ob_prop, function(name, def){
          $('#check').append('<input class="ob_prop" style="display:block;" type="checkbox" value="'+def+'" name="'+name+'">'+name+'</option>');
        });
        // Functionalities on checkboxes
        $(".ob_prop").change(function() {
          if($(this).is(":checked")) { // if a checkbox is checked call for the data and draw the line
            observations (offeName, procName, $(this).val(), start, end, false);
          } else { // if a checkbox is unchecked remove the trace from the plot
            var check = $(this).attr('name');
            $.each(traces, function(key, val){
              if (val.name == check) {
                traces.splice(key,1);
                Plotly.redraw('plotly');
              }
            });
          }
        });

        // set checked the first observed property - get the first observed property definition and call the observations function
        $(".ob_prop").first().attr('checked', true);
        observations (offeName, procName, $(".ob_prop").first().val(), start, end, true);
      };
    });

    function observations (offeName, procName, opDef, start, end, newPlot) {

        $.ajax({
            url: "?q=observations_graph_callback",
            type: "POST",
            data: {offename: offeName, procname: procName, opdef: opDef, start: start, end: end},
            success: observationSuccess,
            complete: completeFunction,
        });

        function observationSuccess (data, textStatus, jqXHR) {
          // console.log(data);
          //
          // console.log(data.result.DataArray);
          var elements = data.result.DataArray.elementCount;
          // use d3.transpose to format the returned array in a way usable with plotly.js
          var transposed = Plotly.d3.transpose(data.result.DataArray.values);
          // transposed[0] contains the list of dates
          // transposed[n] contains the list of values or quality index
					// var traces = []; // array of traces
          var uoms = [];   // array of unit of measure
          //
          // Convert iso8601 dates to plotly dates
          // This is manadatory to use the type:"date" on xaxis
          //
          var timearr = []
          $.each(transposed[0], function(index, value){
            // TODO the date returned from istsos is +00:00 the date calculate is ported to the local system date
            // console.log(value); // 2015-05-03T14:40:00+00:00
            var d = new Date(value);
            // console.log(d); // Sun May 03 2015 16:40:00 GMT+0200 (CEST)
            //var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();
            var datestring = Drupal.behaviors.istsosUtils.ISODateString(d);
            timearr.push(datestring)
          });
          //
          // Insert data and legend names
          // TODO add data quality index
          //
          for (var count = 0; count<elements; count++ ) {
							if (count%2 != 0){
                //console.log(data.result.DataArray.field[count]);
                var obj = {"x" : timearr, "y" : transposed[count] , "name" : data.result.DataArray.field[count].name}
                // var obj = {"x" : transposed[0], "y" : transposed[count] , "name" : data.result.DataArray.field[count].name}
								traces.push(obj);
                uoms.push(data.result.DataArray.field[count].uom);
							}
					}
          //TODO the layout must be difined once
          var layout = {
            //hovermode: "x", //
            // xaxis: {range:['2015-08-13T00:00:00Z', '2015-09-14T00:00:00Z']}
            xaxis: {title:'Time', type:"date", tickformat:"%Y/%m/%d", hoverformat: "%y/%m/%d %X %Z"},
            // xaxis: {title:'Time', tickformat:"%Y/%m/%d"},
            yaxis: {title: uoms[0]},
            //margin: {t: 20},
            showlegend: true, // fixed legend
          };

          // jsonData = JSON.stringify({"data" : traces} , null ,4)
          // console.log(jsonData);


          if (newPlot) {
            console.log(traces)
            // traceUids.append()
            Plotly.newPlot('plotly', traces, layout); // perform newplot for the first observation
            // Plotly.newPlot('plotly', traces);
          } else {
            console.log(traces)
            // Plotly.plot('plotly', traces, layout); // perform plot for other observation
            // Plotly.addTraces('plotly', obj); //
            Plotly.redraw('plotly');
            // Plotly.newPlot('plotly', traces, layout);
          }
        };

        function completeFunction () {
          // Plotly.newPlot('plotly', jsonData , {title: 'Plotting CSV data from AJAX call'});
        };

      };

  }
}

})(jQuery);
