(function ($) {

Drupal.behaviors.istsosObservations = {
  attach: function (context, settings) {
		console.log("na load");
		
		$(".procedure").click(function () {
	    console.log("procedure clicked");
	    console.log ($(this).html()); // procedure name
	    var procName = encodeURIComponent($(this).html());
			$('#procedurename').empty();	    
  		$('#procedurename').html(procName);	    
	    console.log (encodeURIComponent($( "input[name='offering_name']" ).val())); // offering name
	    var offeName = encodeURIComponent($( "input[name='offering_name']" ).val());
	    
	    $('#ajaxLoader').toggle();

	    $.ajax({
					url: "?q=observations_graph_callback",
					type: "POST",
//					async: false,
					data: {offename: offeName, procname: procName},
					success: successFunction,
					//error: errorFunction
					complete: completeFunction,
			});
		
			function successFunction (data, textStatus, jqXHR) {
					console.log('success');
//					console.log(data);
					// Reset the GRAPH
					$('#legend').empty();
					$('#chart').empty();

					var dataObject = JSON.parse(data);
//					console.log(dataObject);		
//					console.log(dataObject.data[0].result.DataArray.values);
//					console.log(dataObject.data[0].result.DataArray.elementCount);
				var elements = dataObject.data[0].result.DataArray.elementCount;
					
					
//					formatDate = d3.time.format.iso;//TODO ??????
//					formatDate = d3.time.format.utc("%Y-%m-%d %H:%M:%S");
					
//					a = formatDate.parse('2012-02-01T20:00:00').getTime();
					
					var transposed = d3.transpose(dataObject.data[0].result.DataArray.values);

					/* Create time series foreach element inside the time series */
					var seriesData = new Array; // space for series data 
					var tseries    = new Array; // space for series
					
					for (var count = 0; count<elements; count++ ) {
							if (count%2 != 0){
//								console.log('count');
//								console.log(count);
								// create the series Data array using the 
								seriesData.push([ transposed[0], transposed[count] ]);
								
								// Add a new object with name property
								var serobj  = new Object();
								serobj['name'] = dataObject.data[0].observedProperty.component[count];
								tseries.push(serobj);
							}
														 						
					}
					

					
					var keys = ['x', 'y']; // main keys for ricksahw graph
					
				  collection = seriesData.map(function (e) {
							var array = Array();							
							e[0].forEach(function (key, i) {
									var obj  = new Object();
//									console.log(e[0][i]);
//									obj['x'] = Date.parse( e[0][i].split('.')[0] );
									obj['x'] = Date.parse( e[0][i] )/1000;
//									obj['x'] = Date.parse( e[0][i]);
									if (e[1][i] != "None") {
										obj['y'] = parseFloat(e[1][i]);
									} else {
										obj['y'] = 0; // TODO gestire i Nan values
									}
									array[i] = obj; 
							});
							return array;
					});

//				console.log('collection');
//					console.log(collection.length);

//					var palette = new Rickshaw.Color.Palette( { scheme: 'classic9' } );
					var palette = new Rickshaw.Color.Palette();
					
					tseries.forEach	(function (key, i) {
						tseries[i].data = collection[i];
						tseries[i].color = palette.color();
					});
					
//					console.log(tseries);
										
			var graph = new Rickshaw.Graph( {
        element: document.querySelector("#chart"),
        width: 900,
				height: 500,
        renderer: 'line',
//        stroke: true,
//        preserve: true,
				series: tseries
			} );

			graph.render();
			
			var preview = new Rickshaw.Graph.RangeSlider( {
				graph: graph,
				element: document.getElementById('preview'),
			} );

			var hoverDetail = new Rickshaw.Graph.HoverDetail( {
				graph: graph,
				xFormatter: function(x) {
					return new Date(x * 1000).toString();
				}
			} );

//									var annotator = new Rickshaw.Graph.Annotate( {
//										graph: graph,
//										element: document.getElementById('timeline')
//									} );

			var legend = new Rickshaw.Graph.Legend( {
				graph: graph,
				element: document.getElementById('legend')

			} );

			var shelving = new Rickshaw.Graph.Behavior.Series.Toggle( {
				graph: graph,
				legend: legend
			} );

//									var order = new Rickshaw.Graph.Behavior.Series.Order( {
//										graph: graph,
//										legend: legend
//									} );

			var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
				graph: graph,
				legend: legend
			} );

			var smoother = new Rickshaw.Graph.Smoother( {
				graph: graph,
				element: document.querySelector('#smoother')
			} );

			var ticksTreatment = 'glow';

			var xAxis = new Rickshaw.Graph.Axis.Time( {
				graph: graph,
				ticksTreatment: ticksTreatment,
				timeFixture: new Rickshaw.Fixtures.Time.Local()
			} );

			xAxis.render();

			var yAxis = new Rickshaw.Graph.Axis.Y( {
				graph: graph,
				tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
				ticksTreatment: ticksTreatment
			} );

			yAxis.render();

			/* file extensions.js */	
			var controls = new RenderControls( {
//				element: document.getElementById('side_panel'),
				element: document.querySelector('#istsos-sidebar-form'),
				graph: graph
			} );
			
//			$('#ajaxLoader').toggle();

			};
	  
	  	function completeFunction () {
      	$('#ajaxLoader').toggle(); //it's hidden again
            //other oncomplete stuff
			};

	    			
		});
	}
}	

})(jQuery);
