<?php

function istsos_services () {
#  return drupal_get_form('aunica_login_configuration_form');
  // $form = drupal_get_form('istsos_services_form');
  // $output = drupal_render($form);
  // return $output;
  return drupal_get_form('istsos_services_form');
}

function istsos_services_form($form, &$form_state) {
	$url     = variable_get('istsos_url');
	$form = array();

	$services = drupal_http_request($url.'/wa/istsos/services');
	if (in_array( $services->code, array(200, 304))) {
		 $t = drupal_json_decode($services->data);
#		 drupal_set_message("Services <pre>".print_r($t,TRUE)."</pre>");
			if ($t['success']){
				$form['message'] = array(
					'#markup' => $t['message'],
					'#prefix' => '<div>',
					'#suffix' => '</div>',
				);

				$options = array();
				foreach($t['data'] as $value) {
					$options[$value['service']] =  $value['service'];
				}

				$form['istsos_service'] = array(
					'#type' => 'select',
				  '#title' => t('Select service'),
				  '#options' => $options,
				  '#default_value' => variable_get('istsos_service',''),
				  '#description' => t('Select a service to work with.'),
				);


			}
	}
	else {
		drupal_set_message("Can't get information about istsos server", 'error');
	}

	return system_settings_form($form);
}
