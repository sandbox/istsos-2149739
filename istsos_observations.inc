<?php

	/* Big data file upload */	
	drupal_set_time_limit ( 100 );

function istsos_observations($node){
  return drupal_get_form("istsos_observations_form", $node);
}

function istsos_observations_form($form,&$form_state, $node){

	if (!isset($form_state['stage'])) $form_state['stage'] = 'parse_file'; 

	switch ($form_state['stage']) {
    
    case 'parse_file':
      return istsos_observations_form_prim($form, $form_state, $node);
     break;  
     
    case 'show_observation':
      return istsos_add_observations_form($form, $form_state, $node);
     break;  
  
    default:
      return istsos_observations_form_prim($form, $form_state, $node);
     break; 
  }
  
  return $form;
}

function istsos_observations_form_prim($form,&$form_state, $node){
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	

	$form['import'] = array(
    '#type' => 'managed_file',
    '#title' => t('File'),
    '#description' => t('Upload observation file, allowed extensions: txt'),
    '#upload_location' => 'public://observations/', 
    '#upload_validators' => array( 
    	'file_validate_extensions' => array('txt'), 
    ),
	);
	
	
	$form['foi_name'] = array('#type' => 'hidden', '#value' => $node->title);

	$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Verify data'), '#submit' => array('observation_set_stage'));


	  	 
  return $form;
}

function istsos_add_observations_form($form, $form_state, $node){

	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
	$enodet  = rawurlencode($node->title); 
	
	$uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array( ':fid' => $form_state['input']['import']['fid'], ))->fetchField(); 
	if(!empty($uri)) { 
		if(file_exists(drupal_realpath($uri))) { 
			// istantite the matlab class for store the data
			$mt = new mat(drupal_realpath($uri));

			
			$form['series'] = array('#type' => 'hidden', '#value' => serialize($mt));
			$form['submit'] = array('#type' => 'submit', '#weight' => 20, '#value' => t('Insert'), '#submit' => array('istsos_observations_form_submit'));

			$num_val = count($mt->value);
			$form['example_data'] = array(
				'#markup' => '<h2>Example of data are going to be uploaded. Total values : <b>'.$num_val.'</b></h2>',
			);
			/* Example of data that will be insert */
			$header = array(
				'time'  => t('Time'),
				'value' => t('Value'),
			);

			$options = array();
			for($i=0; $i<=4; $i++) {
					$options[$i] =array(
						'time'  => $mt->time[$i],
						'value' => $mt->value[$i],
					);	
			}

			$form['table'] = array(
				'#theme' => 'table',
				'#header' => $header,
				'#rows' => $options,
				'#empty' => t('No measures forund'),
			);


			$procedures = drupal_http_request($url.'/wa/istsos/services/'.$service.'/offerings/'.$enodet.'/procedures/operations/memberslist');
			if (in_array( $procedures->code, array(200, 304))) {
				$t = drupal_json_decode($procedures->data);
#				drupal_set_message("Procedures inside offering <pre>".print_r($procedures,TRUE)."</pre>");
				
				$form['existing_procedure'] = array(
					'#markup' => '<h2>Select the procedure you want to attach the data.</h2>',
				);
				
				$proc_header = array(
					'proc_name'  => t('Procedure Name'),
				);

				$proc_options = array();
				foreach ($t['data'] as $value){
					$proc_options[$value['name']] =array(
						'proc_name' => array(
						  'data' => array(
							  '#markup'	=> $value['name'],
						  ),
						),

					);
				}

				$form['procedure_table'] = array(
			    '#type' => 'tableselect',
					'#header' => $proc_header,
			    '#options' => $proc_options,
					'#multiple' => FALSE,
					'#empty' => t('No procedures found'),
				);
			}
			
		}
	}
	return $form;
}

function observation_set_stage ($form,&$form_state){
	$form_state['stage'] = 'show_observation';
	$form_state['stage_one']['values'] = $form_state['values'];
	$form_state['rebuild'] = TRUE;
}

function istsos_observations_form_submit($form, &$form_state) {
	$url     = variable_get('istsos_url');
	$service = variable_get('istsos_service');
#	drupal_set_message("<pre>".print_r($form_state['values'],TRUE)."</pre>");
	
	$selected_procedure_name = $form_state['values']['procedure_table'];

	/* Get details of the selected procedure for obtain the parameters to construct the request */
	/* @ assigned sensor id */
	/* @ epsg */
	/* @ coordinates */
	/* @ components or outputs */	
	$procedure = drupal_http_request($url.'/wa/istsos/services/'.$service.'/procedures/'.$selected_procedure_name);
	if (in_array( $procedure->code, array(200, 304))) {
		$t = drupal_json_decode($procedure->data);
#		drupal_set_message("Procedure <pre>".print_r($t,TRUE)."</pre>");

		if ($t['success']){	// Success to retrieve th procedure informations
  		
  		/* Getting data to formulate the request */
  		$foi_name = $form_state['stage_one']['values']['foi_name'];
  		$asi = $t['data']['assignedSensorId'];

  		$epsg = $t['data']['location']['crs']['properties']['name'];
  		$x =  $t['data']['location']['geometry']['coordinates'][0];
  		$y =  $t['data']['location']['geometry']['coordinates'][1];
  		$z =  $t['data']['location']['geometry']['coordinates'][2];

  		$component = $t['data']['outputs'];

			/* Unserialize the series */  		
			$mt = unserialize($form_state['values']['series']);


 		// see /usr/local/istsos/walib/istsos/services/services.py line 556 for the correct parameters 

		// Actually the procedure has
		// system_id system and location->properties->name settled to the drupal node title 
		// this is due to the way we create the procedure in istos_tab.inc 

 		$data['AssignedSensorId'] = $asi; 
 		$data['ForceInsert'] = 'true';
 		$data['Observation'] = array(
 			'AssignedSensorId' => $asi,
 			'procedure' => "urn:ogc:def:procedure:x-istsos:1.0:".$selected_procedure_name,
 			'name' => $selected_procedure_name
  	);

 		$geom_string = "&lt;gml:Point srsName=".$epsg."'&gt;&lt;gml:coordinates&gt;".$x.",".$y.",".$z."&lt;/gml:coordinates&gt;&lt;/gml:Point&gt;";
# 		drupal_set_message("geometry <pre>".print_r($geom_string,TRUE)."</pre>");	    		
  	$data['Observation']['featureOfInterest'] = array(
 			"geom" => $geom_string,
 			"name" =>	"urn:ogc:def:feature:x-istsos:1.0:Point:".$foi_name // This is the foi name that courrently is the node title
 		);
  	
  	$dim = count( $component );
		$data['Observation']['observedProperty']['CompositePhenomenon'] = array(
      		"id" => "comp_126", 					
      		"dimension" => (string)$dim  	
    );
#		
#		/* the number of components are token from the procedure */		
#		
 		$f_component = array();
 		$field = array();
 		
 		foreach ($component as $value){
 			$f_component[] = $value['definition'];
 			if ( !empty($value['uom']) ){
 				$field[] = array("definition" => $value['definition'], "name" => $value['name'], "uom" => $value['uom']);
 			} else {
	 			$field[] = array("definition" => $value['definition'], "name" => $value['name']);
 			}	
 		}
 		
 		$data['Observation']['observedProperty']['component'] = $f_component;

 		$val= array();
 		foreach ($mt->time as $key => $time){
 			$val[] = array($time, $mt->value[$key]);
 		}


		$data['Observation']['result']['DataArray'] = array(
			"elementCount" => "2",
			"values" => $val,
			"field" => $field
		);
  	
  	

  	$data['Observation']['samplingTime'] = array(
      	"beginPosition" => $mt->time[0], 
		    "endPosition" => end($mt->time)
    );

 		$json = my_json_encode($data); // To encode the geom_string including special chars
# 		$json = drupal_json_encode($data);
# 		drupal_set_message("<pre>".print_r($json,TRUE)."</pre>");
 		
				 		$options = array(
				    'method' => 'POST',
				    'data' => my_json_encode($data),
				    'headers' => array('Content-Type' => 'application/json'),
				  );
				 		
					$observations = drupal_http_request($url.'/wa/istsos/services/'.$service.'/operations/insertobservation', $options);
					#	drupal_set_message("INSERT <pre>".print_r($obprop,TRUE)."</pre>");
						if (in_array( $observations->code, array(200, 304))) {
							 $t = drupal_json_decode($observations->data);
#							 drupal_set_message("INSERT <pre>".print_r($t,TRUE)."</pre>");
#							 drupal_set_message("<pre>".print_r($t['message'],TRUE)."</pre>");
								if ($t['success']){
										drupal_set_message($t['message']);
								}
						}
						else {
							drupal_set_message("Can't get information about istSOS server", 'error');
						}	  	

		} else {
			drupal_set_message('You have to set the procedure before to add observations', 'error');
		}



	} else {
		drupal_set_message("Can't get information about the procedure", 'error');
	}	
}


/* matlab text parser */

class mat {
#	drupal_set_time_limit ( 100 );

	private $_fileName;
#	private $_lastTag = '';

  public $unix_epoch_datenum = 719529;

	public $name;
	public $author;
	public $created;
	public $id_type;
	public $marker_id;
	public $origin;
	public $scenario;
	public $frequency;
	public $time;
	public $value;
	

	
	public function __construct($fileName) {
		$this->_fileName = $fileName;
		$this->parse();
	}
	
	public function parse() {
		$tab = $this->prepareFile();
		foreach($tab as $line){
			preg_match('/:(.*?)\n/', $line , $tag);

			switch(trim($tag[1])) {
			case 'time':
				$this->fill_time($line);
				break;
			case 'value':
				$this->fill_value($line);
				break;


			default:
				break;
			}
		}
	
	}

	private function fill_value($value) {

		preg_match('/# columns: (-?\d+)/', $value , $columns);
		

		$val = substr($value, strpos($value, 'columns'));
		preg_match_all('/ (.*?)\n/', $val , $array);
		
		$total = array();
		foreach ($array[1] as $key => $values) {

			if ($key == 0){
#				echo "columns : ".$values."<br />";
			} else {
#					echo 'values <pre>';
#					print_r($values);
#					echo '</pre>';

				$total[]=$values;
			}
			$this->value = $total; 
		}
	}

	private function fill_time($value) {
		
		preg_match('/# columns: (-?\d+)/', $value , $columns);
		
		$val = substr($value, strpos($value, 'columns'));
		
		preg_match_all('/ (.*?)\n/', $val , $array);
#		echo "<h2>".$tag[1]."</h2>";
		
		$total_times = array();
		foreach ($array[1] as $key => $values) {

			if ($key == 0){

			} else {
#					echo 'values <pre>';
#					print_r($values);
#					echo '</pre>';

				preg_match('/(.*?) /', $values , $ts);

					$days_before_epoch = $this->unix_epoch_datenum-$ts[1];
					$unixDate = -$days_before_epoch * 86400;
					$total_times[] = date("c", $unixDate);

			}
			$this->time = $total_times; 
		}
	}


	private function prepareFile() {
		$tab = file($this->_fileName);
		$tags = array();
		

		$tmp = '';
		foreach($tab as $line) {			

			if (substr($line,0,6) == '# name' && $tmp != ''){
				$tags[] = $tmp;
				$tmp = '';
				
			}	
			$tmp .= $line;
		}
		$tags[] = $tmp;
		return $tags;
	}		
}



function my_json_encode($arr)
{
        //convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding
        array_walk_recursive($arr, function (&$item, $key) { if (is_string($item)) $item = mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8'); });
        return mb_decode_numericentity(json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');

}
